<?php 
namespace Drupal\auto_promote\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AutoPromoteSettingsForm extends ConfigFormBase {

  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  protected function getEditableConfigNames() {
    return ['auto_promote.settings'];
  }

  public function getFormId() {
    return 'auto_promote_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('auto_promote.settings');

    // Module Description
    $form['description'] = [
        '#markup' => $this->t('Configure settings for the Auto Promote module. Select content types and user roles for automatic promotion.'),
    ];

    // Collapsible Details for Content Types
    $form['content_type_details'] = [
        '#type' => 'details',
        '#title' => $this->t('Content Type Settings'),
        '#open' => TRUE, // Set to FALSE to have this section collapsed by default
    ];

    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $form['content_type_details']['content_types'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Content Types'),
        '#description' => $this->t('Select the content types that will be auto-promoted.'),
        '#options' => array_map(function ($content_type) {
            return $content_type->label();
        }, $content_types),
        '#default_value' => $config->get('content_types') ?: [],
    ];

    // Collapsible Details for User Roles
    $form['user_role_details'] = [
        '#type' => 'details',
        '#title' => $this->t('User Role Settings'),
        '#open' => TRUE, // Set to FALSE to have this section collapsed by default
    ];

    $user_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $form['user_role_details']['user_roles'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('User Roles'),
        '#description' => $this->t('Select the author roles that will be auto-promoted.'),
        '#options' => array_map(function ($user_role) {
            return $user_role->label();
        }, $user_roles),
        '#default_value' => $config->get('user_roles') ?: [],
    ];

    return parent::buildForm($form, $form_state);
}

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('auto_promote.settings')
      ->set('content_types', array_filter($form_state->getValue('content_types')))
      ->set('user_roles', array_filter($form_state->getValue('user_roles')))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
